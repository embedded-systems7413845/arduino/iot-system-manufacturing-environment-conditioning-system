/* Importing Libraries */
#include <LiquidCrystal.h>
#include <DHT.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <ThingSpeak.h>


/* Custom function declarations*/
float arrSum(float arr[], int size);
int intArrSum(int arr[], int size);
float arrMean(float arr[], int size);
int sendGetRequest(HTTPClient& http, String url);
void siren(int pin);


/* Control Variables */
// WiFi connection
const char* ssid     = "SSID";
const char* password = "PASSWORD";

// ThingSpeak Channel
#define CHANNEL_ID xxxxxxx
#define CHANNEL_WRITE_API_KEY "xxxxxxxxxxxxxxxx"
#define CHANNEL_READ_API_KEY "xxxxxxxxxxxxxxxx"

// 1602 Display digital pins
const int rs = 2, en = 4, d4 = 9, d5 = 10, d6 = 11, d7 = 12;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// RGB LEDs
#define RED_LED 18
#define GRN_LED 20
#define BLU_LED 22

// DHT11
#define DHT_PIN 24         // Digital pin
#define DHT_TYPE DHT11     // Type
DHT dht(DHT_PIN, DHT_TYPE); // Init DHT sensor

// MQ-2
#define MQ2_PIN A0

// Init KY-006
#define BUZZER_PIN 23 // Digital pin connected to the KY-006 Buzzer


/* Declare variables*/
// Clients
WiFiClient client;
HTTPClient http;

// URLs
String redURL = "https://api.thingspeak.com/channels/" + String(CHANNEL_ID) + "/fields/5/last?api_key=" + CHANNEL_READ_API_KEY;
String grnURL = "https://api.thingspeak.com/channels/" + String(CHANNEL_ID) + "/fields/6/last?api_key=" + CHANNEL_READ_API_KEY;
String bluURL = "https://api.thingspeak.com/channels/" + String(CHANNEL_ID) + "/fields/7/last?api_key=" + CHANNEL_READ_API_KEY;
int response = 0;

// Timer & Indicators
unsigned int countdown = 0;
unsigned int er = 0;

// Sensors output
float h;
float t;
float f;
float hif;
float hic;
int se;

// Tresholds
float h_HTr = 50;
float h_LTr = 20;
float t_HTr = 25;
float t_LTr = 15;
float se_Tr = 3000;

// Anomalies
String anomalies = "";
String tmp;
int alertCases;

// ThingSpeak
int arrIndex = 0;
const int bufferSize = 16;
float hArr[bufferSize];
float tArr[bufferSize];
float seArr[bufferSize];
int anomaliesArr[bufferSize];


/* Setup sensors*/
void setup() {
  // WiFi connection
  Serial.begin(9600);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Wait for WiFi...");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.print(WiFi.localIP());

  // ThingSpeak connection
  ThingSpeak.begin(client);

  // 1602 Display
  lcd.begin(16, 2);

  // LEDs
  pinMode(RED_LED, OUTPUT);
  pinMode(GRN_LED, OUTPUT);
  pinMode(BLU_LED, OUTPUT);

  // DHT11
  dht.begin();

  // MQ-2
  pinMode(A0, INPUT);

  // KY-006
  pinMode (BUZZER_PIN, OUTPUT);
}


/* Program */
void loop() {

  /* ThingSpeak Read Data */
  // Red LED
  response = sendGetRequest(http, redURL);
  if(response != -1) {
    digitalWrite(RED_LED, response);
  }

  // Green LED
  response = sendGetRequest(http, grnURL);
  if(response != -1) {
    digitalWrite(GRN_LED, response);
  }

  // Blue LED
  response = sendGetRequest(http, bluURL);
  if(response != -1) {
    digitalWrite(BLU_LED, response);
  }


  /* Sensor Data Acquisition */
  // DHT11
  h = dht.readHumidity();
  t = dht.readTemperature();
  tmp = String(t).substring(0, String(t).length()-1);
  f = dht.readTemperature(true);
  hif = dht.computeHeatIndex(f, h);
  hic = dht.computeHeatIndex(t, h, false);
  
  // MQ-2
  se = map(analogRead(MQ2_PIN), 0, 4095, 0, 9999);
  
  
  /* Checking Sensors Statuses */
  if (isnan(h) || isnan(t) || isnan(f)) {
    er = 1;
  }
  if (isnan(se)) {
    er += 2;
  }
  if (er == 1) {
    lcd.setCursor(0, 0);
    lcd.print("                ");
    lcd.setCursor(0, 1);
    lcd.print("                ");
    lcd.setCursor(0, 0);
    lcd.print("DHT SNSR ER");
    delay(10000);
    ESP.restart();
  }
  if (er == 2) {
    lcd.setCursor(0, 0);
    lcd.print("                ");
    lcd.setCursor(0, 1);
    lcd.print("                ");
    lcd.setCursor(0, 0);
    lcd.print("MQ-2 SNSR ER");
    delay(10000);
    ESP.restart();
  }
  if (er == 3) {
    lcd.setCursor(0, 0);
    lcd.print("                ");
    lcd.setCursor(0, 1);
    lcd.print("                ");
    lcd.setCursor(0, 0);
    Serial.println("DHT SNSR ER");
    lcd.setCursor(0, 1);
    Serial.println("MQ-2 SNSR ER");
    delay(10000);
    ESP.restart();
  }
  er = 0;

  // Anomaly Detection
  anomalies = "";
  alertCases = 0;
  if (h < h_LTr) {
    anomalies += "L Hm ";
    alertCases++;
  }
  if (h > h_HTr) {
    anomalies += "Hg Hm ";
    alertCases++;
  }
  if (t < t_LTr) {
    anomalies += "L tmp ";
    alertCases++;
  }
  if (t > t_HTr) {
    anomalies += "Hg tmp ";
    alertCases++;
  }
  if(se > se_Tr) {
    anomalies += " SE ";
    alertCases++;
  }

  // ThingSpeak data batch
  hArr[arrIndex] = h;
  tArr[arrIndex] = t;
  seArr[arrIndex] = se;
  anomaliesArr[arrIndex] = alertCases;
  arrIndex = (arrIndex + 1) % bufferSize; // Circlar buffer

  // ThingSpeak channel upload
  if(countdown >= 16000) {
    countdown = 0;
    ThingSpeak.setField(1, arrMean(hArr, bufferSize));
    ThingSpeak.setField(2, arrMean(tArr, bufferSize));
    ThingSpeak.setField(3, arrMean(seArr, bufferSize));
    ThingSpeak.setField(4, intArrSum(anomaliesArr, bufferSize));
    ThingSpeak.writeFields(CHANNEL_ID, CHANNEL_WRITE_API_KEY);
  }
  

  /* Feedback */
  // LCD
  lcd.setCursor(0, 0);
  lcd.print("                ");
  lcd.setCursor(0, 0);
  lcd.print("H");
  lcd.print((int)h);

  lcd.print(" T");
  lcd.print(tmp);

  lcd.print(" SE");
  lcd.print(se);

  if (anomalies != "") {
    // KY-006
    siren(BUZZER_PIN);

    // LCD
    lcd.setCursor(0, 1);
    lcd.print("                ");
    lcd.setCursor(0, 1);
    lcd.print(anomalies);
    delay(1000);
  }
  else{
    // LCD
    lcd.setCursor(0, 1);
    lcd.print("                ");
    delay(2000);
  }

  // Timer
  countdown += 2000;
}


/* Custom Functions */
// Array Sum
float arrSum(float arr[], int size) {
  float sum = 0;
  for (int i = 0; i < size; i++) {
    sum += arr[i];
  }
  return sum;
}

int intArrSum(int arr[], int size) {
  int sum = 0;
  for (int i = 0; i < size; i++) {
    sum += arr[i];
  }
  return sum;
}

// Array Mean
float arrMean(float arr[], int size) {
  if (size == 0) {
    return 0;
  }
  float sum = arrSum(arr, size);
  return sum / size;
}

// sendGetRequest()
int sendGetRequest(HTTPClient& http, String url) {
  http.begin(url);
  int httpCode = http.GET();
  int response = -1;
  if (httpCode > 0) {
    String payload = http.getString();
    response = payload.toInt();
  }
  http.end();
  return response;
}

// KY-006 Alert Sound
void siren(int pin) {
  int lowFrequency = 300;
  int highFrequency = 1000;
  int frequency = lowFrequency;
  int duration = 100;
  for (int i = 0; i < 10; i++) {
    tone(pin, frequency, duration);
    delay(duration);
    // Toggle between low and high frequencies
    if (frequency == lowFrequency) {
      frequency = highFrequency;
    } else {
      frequency = lowFrequency;
    }
  }
}
