%% Declarations
% Current date
currentDate = datetime('now', 'Format', 'dd/MM/yyyy');

% Channel ID for PharmaSentinel
channelID = xxxxxxx;

% ThingSpeak Read API Key
readApiKey = "xxxxxxxxxxxxxxxx";

% ThingSpeak alerts API key
alertApiKey = "TAKxxxxxxxxxxxxxxxx";

% Alert URL for HTTP call
alertUrl="https://api.thingspeak.com/alerts/send";

% webwrite uses weboptions to add required headers.  Alerts needs a ThingSpeak-Alerts-API-Key header.
options = weboptions("HeaderFields", ["ThingSpeak-Alerts-API-Key", alertApiKey ]);

% Email subject
alertSubject = sprintf("PharmaSentinel Daily Report %s", char(currentDate));

% Read the recent data.
hmData = thingSpeakRead(channelID, 'ReadKey', readApiKey, 'NumDays',1,'Fields',1);
tmpData = thingSpeakRead(channelID, 'ReadKey', readApiKey, 'NumDays',1,'Fields',2);
seData = thingSpeakRead(channelID, 'ReadKey', readApiKey, 'NumDays',1,'Fields',3);
anomaliesData = thingSpeakRead(channelID, 'ReadKey', readApiKey, 'NumDays',1,'Fields',4);


% Checking data reading if empty
alertBody = "Data summary for today:" + newline;

alertBody = alertBody + "Humidity:" + newline;
if isempty(hmData)
    alertBody = alertBody + " --- > Humidity data missing" + newline + newline;
else
    avg = mean(hmData);
    low = min(hmData);
    high = max(hmData);
    alertBody = alertBody + "Average: " + avg + "%" + newline + "Lowest: " + low + "%" + newline + "Highest: " + high + "%" + newline + newline;
end


alertBody = alertBody + "Temperature:" + newline;
if isempty(tmpData)
    alertBody = alertBody + " --- > Temperature data missing" + newline + newline;
else
    avg = mean(tmpData);
    low = min(tmpData);
    high = max(tmpData);
    alertBody = alertBody + "Average: " + avg + " Celcius" + newline + "Lowest: " + low + " Celcius" + newline + "Highest: " + high + " Celcius" + newline + newline;
end


alertBody = alertBody + "Gas concentration:" + newline;
if isempty(seData)
    alertBody = alertBody + " --- > Gas concentration data missing" + newline + newline;
else
    avg = mean(seData);
    low = min(seData);
    high = max(seData);
    alertBody = alertBody + "Average: " + avg + " PPM" + newline + "Lowest: " + low + " PPM" + newline + "Highest: " + high + " PPM" + newline + newline;
end


alertBody = alertBody + "Anomalies per each 16 seconds:" + newline;
if isempty(anomaliesData)
    alertBody = alertBody + " --- > Anomalies were not recorded" + newline + newline;
else
    tot = sum(anomaliesData);
    avg = mean(anomaliesData);
    low = min(anomaliesData);
    high = max(anomaliesData);
    alertBody = alertBody + "Total anomalies: " + tot + newline + "Average: " + avg + newline + "Lowest: " + low + newline + "Highest: " + high + newline + newline;
end
alertBody = alertBody + "Data Acquisition provided by PharmaSentinel Systems from PharmaSentinel(ltd), Located at FAH HAHBAH!!!";

% Create weboptions with the desired headers
options = weboptions("HeaderFields", ['Thingspeak-Alerts-API-Key', alertApiKey]);

% Send the request
try
    webwrite(alertUrl , "body", alertBody, "subject", alertSubject, options);

    catch someException
    fprintf("Failed to send alert: %s\n", someException.message);
end
