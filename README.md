# PharmaSentinel IoT Monitoring System

PharmaSentinel is an IoT project implemented in a pharmaceutical manufacturing environment. The system utilizes an Arduino board, specifically the NANO ESP32-S3, along with various sensors to monitor crucial parameters such as humidity and temperature. Additionally, the system is equipped to detect the presence of smoke, a critical factor in pharmaceutical manufacturing processes.

## Key Features
- **Sensor Monitoring**: Humidity and temperature are continuously monitored to ensure they remain within specified ranges crucial for pharmaceutical production.
- **Smoke Detection**: The system is designed to identify the presence of smoke, which could potentially impact the manufacturing process negatively.
- **RGB LED Control**: The visualization page features six buttons (two for each LED: on and off) that send POST requests to ThingSpeak, altering the binary fields (Red: 5, Green: 6, Blue: 7) and controlling the corresponding RGB LEDs.

## Importance of Visualized Data
- **Quality Assurance**: Real-time visualization ensures adherence to quality standards, crucial for pharmaceutical products. Despite the cost-effectiveness, PharmaSentinel delivers actionable insights to maintain product quality.

- **Process Optimization**: Affordable sensors provide valuable data for process engineers, supporting continuous improvement and efficiency gains in manufacturing processes.

- **Risk Mitigation**: PharmaSentinel's cost-effective approach enables proactive risk mitigation by detecting anomalies in climate control and smoke. Decision-makers benefit from timely insights to maintain a safe manufacturing environment.

## Data Visualization and Control
- **ThingSpeak Integration**: Data collected from the sensors are sent to a ThingSpeak server for visualization and analysis.
- **ThingSpeak Apps**: Three ThingSpeak apps are utilized:
  - **Plugins**: HTML buttons are created to control RGB LED lights on the visualization page. HTML buttons on the visualization page demonstrate the system's responsiveness to external commands using HTML and Javascript. This functionality is achieved with an economical setup, showcasing the project's practicality.
  - **Matlab Analysis**: Collaborating with TimeControl, this app generates recurring email summaries of the last 24 hours' data.
  - **TimeControl**: Enables scheduled execution of the Matlab script to compute data summaries and send emails at predefined intervals.

This PharmaSentinel project demonstrates the integration of IoT devices, sensor monitoring, and cloud-based analytics to ensure the optimal conditions in a pharmaceutical manufacturing environment.
